﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField]
    private Transform mSrcLeftBottom;
    [SerializeField]
    private Transform mSrcRightTop;

    [SerializeField]
    private Transform mDstLeftBottom;
    [SerializeField]
    private Transform mDstRightTop;

    [SerializeField]
    private float mTimeTermRangeMin;
    [SerializeField]
    private float mtimeTermRangeMax;

    private float mRemainTime;

    //[SerializeField]
    //private Transform mFrontAim;
    private void OnDrawGizmos()
    {
        if (mSrcLeftBottom != null && mSrcRightTop != null)
        {
            Gizmos.color = Color.green;
            Vector3 srcLeftBottom = mSrcLeftBottom.position;
            Vector3 srcRightTop = mSrcRightTop.position;
            Gizmos.DrawCube(new Vector3((srcLeftBottom.x + srcRightTop.x) * 0.5f,
                (srcLeftBottom.y + srcRightTop.y) * 0.5f,
                (srcLeftBottom.z + srcRightTop.z) * 0.5f),
                new Vector3(Mathf.Abs(srcRightTop.x - srcLeftBottom.x),
                Mathf.Abs(srcRightTop.y - srcLeftBottom.y),
                Mathf.Abs(srcRightTop.z - srcLeftBottom.z)));
        }

        if (mDstLeftBottom != null && mDstRightTop != null)
        {
            Gizmos.color = Color.red;
            Vector3 dstLeftBottom = mDstLeftBottom.position;
            Vector3 dstRightTop = mDstRightTop.position;
            Gizmos.DrawCube(new Vector3((dstLeftBottom.x + dstRightTop.x) * 0.5f,
                (dstLeftBottom.y + dstRightTop.y) * 0.5f,
                (dstLeftBottom.z + dstRightTop.z) * 0.5f),
                new Vector3(Mathf.Abs(dstRightTop.x - dstLeftBottom.x),
                Mathf.Abs(dstRightTop.y - dstLeftBottom.y),
                Mathf.Abs(dstRightTop.z - dstLeftBottom.z)));
        }
    }

    private void Update()
    {
        if (mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            mRemainTime = Random.Range(mTimeTermRangeMin, mtimeTermRangeMax);
            Vector3 srcLeftBottom = mSrcLeftBottom.position;
            Vector3 srcRightTop = mSrcRightTop.position;
            Vector3 dstLeftBottom = mDstLeftBottom.position;
            Vector3 dstRightTop = mDstRightTop.position;

            ProjectileHandler.Fire(EProjectileFlags.GUIDED_MISSILE,
                new Vector3(Random.Range(srcLeftBottom.x, srcRightTop.x), Random.Range(srcLeftBottom.y, srcRightTop.y), Random.Range(srcLeftBottom.z, srcRightTop.z)),
                Vector3.zero,
                new Vector3(Random.Range(dstLeftBottom.x, dstRightTop.x), Random.Range(dstLeftBottom.y, dstRightTop.y), Random.Range(dstLeftBottom.z, dstRightTop.z)));

            //ProjectileHandler.Fire(EProjectileFlags.GUIDED_MISSILE,
            //    new Vector3(Random.Range(srcLeftBottom.x, srcRightTop.x), Random.Range(srcLeftBottom.y, srcRightTop.y), Random.Range(srcLeftBottom.z, srcRightTop.z)),
            //    Vector3.zero,
            //    mFrontAim);
        }
    }

}
