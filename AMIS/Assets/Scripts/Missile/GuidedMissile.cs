﻿using UnityEngine;
using System.Collections;

public class GuidedMissile : AMissile
{
    [SerializeField]
    private float mSpeed;
    private float mRemainTime;
    private const float END_TIME = 10.0F;

    protected override void InitializeMissile()
    {
        if (FireType == EFireType.POSITION)
        {
            float distance = Vector3.Distance(transform.position, DstPosition);
            float time = distance / mSpeed;
            Vector3 direction = DstPosition - transform.position;

            direction.Normalize();
            float additionalVelocity_y = Constants.GRAVITY * time * 0.5f;

            Rigidbody.velocity = new Vector3(direction.x * mSpeed, direction.y * mSpeed + additionalVelocity_y, direction.z * mSpeed);
            direction = Rigidbody.velocity;
            float angle_y = -Mathf.Atan2(direction.z, direction.x);
            float angle_z = Mathf.Atan2(direction.y, direction.x);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, angle_y * Mathf.Rad2Deg, angle_z * Mathf.Rad2Deg);
        }
        else if(FireType == EFireType.TRANSFORM)
        {
            mRemainTime = END_TIME;
            Vector3 direction = DstTransform.position - transform.position;
            direction.Normalize();
            Rigidbody.velocity = new Vector3(direction.x * mSpeed, direction.y * mSpeed, direction.z * mSpeed);
            direction = Rigidbody.velocity;
            float angle_y = -Mathf.Atan2(direction.z, direction.x);
            float angle_z = Mathf.Atan2(direction.y, direction.x);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, angle_y * Mathf.Rad2Deg, angle_z * Mathf.Rad2Deg);
        }
    }

    protected override void Fly()
    {
        if (FireType == EFireType.POSITION)
        {
            Vector3 velocity = Rigidbody.velocity;
            Rigidbody.velocity = new Vector3(velocity.x, velocity.y - Constants.GRAVITY * Time.fixedDeltaTime, velocity.z);
            velocity = Rigidbody.velocity;
            float angle_y = -Mathf.Atan2(velocity.z, velocity.x);
            float angle_z = Mathf.Atan2(velocity.y, velocity.x);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, angle_y * Mathf.Rad2Deg, angle_z * Mathf.Rad2Deg);
        }
        else if(FireType == EFireType.TRANSFORM && !IsTriggered)
        {

            if (mRemainTime > 0.0f)
            {
                mRemainTime -= Time.deltaTime;
            }
            else
            {
                IsTriggered = true;
            }
        }
    }
}
