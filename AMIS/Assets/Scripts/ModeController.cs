﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeController : MonoBehaviour
{

    

    // Start is called before the first frame update
    void Start()
    {
        StateManager.State = EState.NONE;
    }

    // Update is called once per frame
    void Update()
    {
        //UserMode로 전환
        if (Input.GetKeyDown(KeyCode.Alpha1) && StateManager.State != EState.USER_MODE)
        {
            StateManager.State = EState.USER_MODE;
        }

        //TrainMode로 전환
        if (Input.GetKeyDown(KeyCode.Alpha2) && StateManager.State != EState.TRAIN_MODE)
        {
            StateManager.State = EState.TRAIN_MODE;
        }

        //TestMode로 전환
        if (Input.GetKeyDown(KeyCode.Alpha3) && StateManager.State != EState.TEST_MODE)
        {
            StateManager.State = EState.TEST_MODE;
        }
    }
}
