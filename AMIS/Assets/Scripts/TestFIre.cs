﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFire : MonoBehaviour
{
    [SerializeField]
    private Transform mTarget;
    [SerializeField]
    private float mTimeTerm;
    private float mRemainTime;

    private void Update()
    {
        if(mRemainTime > 0.0f)
        {
            mRemainTime -= Time.deltaTime;
        }
        else
        {
            mRemainTime = mTimeTerm;
            ProjectileHandler.Fire(EProjectileFlags.GUIDED_MISSILE, transform.position, transform.eulerAngles, mTarget.position);
        }
    }
}
