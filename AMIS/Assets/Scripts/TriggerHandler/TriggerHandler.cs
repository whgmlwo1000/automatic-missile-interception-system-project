﻿using UnityEngine;
using System.Collections.Generic;

public class TriggerHandler<ETriggerFlags> : MonoBehaviour where ETriggerFlags : System.Enum
{
    private Dictionary<ETriggerFlags, Trigger<ETriggerFlags>> mTriggerDict;

    public Collider[] GetColliders(ETriggerFlags triggerFlag)
    {
        return mTriggerDict[triggerFlag].GetColliders();
    }
    public Collider GetTrigger(ETriggerFlags triggerFlag)
    {
        return mTriggerDict[triggerFlag].Collider;
    }
    public void AddEnterEvent(ETriggerFlags triggerFlag, Trigger<ETriggerFlags>.OnTrigger onTrigger)
    {
        mTriggerDict[triggerFlag].OnEnter += onTrigger;
    }
    public void RemoveEnterEvent(ETriggerFlags triggerFlag, Trigger<ETriggerFlags>.OnTrigger onTrigger)
    {
        mTriggerDict[triggerFlag].OnEnter -= onTrigger;
    }
    public void AddExitEvent(ETriggerFlags triggerFlag, Trigger<ETriggerFlags>.OnTrigger onTrigger)
    {
        mTriggerDict[triggerFlag].OnExit += onTrigger;
    }
    public void RemoveExitEvent(ETriggerFlags triggerFlag, Trigger<ETriggerFlags>.OnTrigger onTrigger)
    {
        mTriggerDict[triggerFlag].OnExit -= onTrigger;
    }
    private void Awake()
    {
        mTriggerDict = new Dictionary<ETriggerFlags, Trigger<ETriggerFlags>>();
        Trigger<ETriggerFlags>[] triggers = GetComponentsInChildren<Trigger<ETriggerFlags>>(true);
        for (int i = 0; i < triggers.Length; i++)
        {
            mTriggerDict.Add(triggers[i].TriggerFlag, triggers[i]);
        }
    }
}
