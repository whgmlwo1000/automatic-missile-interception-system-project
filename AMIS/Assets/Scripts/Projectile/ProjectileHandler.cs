﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public enum EProjectileFlags
{
    GUIDED_MISSILE, 
}

[System.Serializable]
public struct ProjectileInfo
{
    public GameObject ProjectilPrefab;
    public int CountOfInst;
}

public class ProjectilePool
{
    public AProjectile[] Projectiles { get { return mProjectilePool; } }

    private int mIndex;
    private AProjectile[] mProjectilePool;

    public ProjectilePool(AProjectile[] projectiles)
    {
        mProjectilePool = projectiles;
    }

    public AProjectile GetNextProjectile()
    {
        int index;
        AProjectile projectile = null;
        for (index = 0; index < mProjectilePool.Length; index++)
        {
            if (!mProjectilePool[mIndex].gameObject.activeInHierarchy)
            {
                projectile = mProjectilePool[mIndex];
                mIndex = (mIndex + 1 >= mProjectilePool.Length ? 0 : mIndex + 1);
                break;
            }
            mIndex = (mIndex + 1 >= mProjectilePool.Length ? 0 : mIndex + 1);
        }
        return projectile;
    }
}

public class ProjectileHandler : MonoBehaviour
{
    private static Dictionary<EProjectileFlags, ProjectilePool> m_projectileDict;

    [SerializeField]
    private ProjectileInfo[] mProjectileInfo;

    public void Awake()
    {
        if (m_projectileDict == null)
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
            m_projectileDict = new Dictionary<EProjectileFlags, ProjectilePool>();

            List<AProjectile> projectileList = new List<AProjectile>();

            for (int i = 0; i < mProjectileInfo.Length; i++)
            {
                projectileList.Clear();
                GameObject instObject = Instantiate(mProjectileInfo[i].ProjectilPrefab, transform);
                instObject.SetActive(false);
                AProjectile projectile = instObject.GetComponent<AProjectile>();
                if (projectile != null)
                {
                    EProjectileFlags projectileFlag = projectile.ProjectileFlag;
                    projectileList.Add(projectile);
                    for (int j = 1; j < mProjectileInfo[i].CountOfInst; j++)
                    {
                        instObject = Instantiate(mProjectileInfo[i].ProjectilPrefab, transform);
                        instObject.SetActive(false);
                        projectile = instObject.GetComponent<AProjectile>();
                        projectileList.Add(projectile);
                    }

                    ProjectilePool pool = new ProjectilePool(projectileList.ToArray());
                    m_projectileDict.Add(projectileFlag, pool);
                }
            }
        }
    }

    public static AProjectile Fire(EProjectileFlags projectileFlag, Vector3 srcPosition, Vector3 srcEulerAngle)
    {
        ProjectilePool pool = m_projectileDict[projectileFlag];
        AProjectile projectile = pool.GetNextProjectile();
        if (projectile != null)
        {
            projectile.gameObject.SetActive(true);
            projectile.Fire(srcPosition, srcEulerAngle);
            return projectile;
        }
        return null;
    }

    public static AProjectile Fire(EProjectileFlags projectileFlag, Vector3 srcPosition, Vector3 srcEulerAngle, Transform dstTransform)
    {
        ProjectilePool pool = m_projectileDict[projectileFlag];
        AProjectile projectile = pool.GetNextProjectile();
        if (projectile != null)
        {
            projectile.gameObject.SetActive(true);
            projectile.Fire(srcPosition, srcEulerAngle, dstTransform);
            return projectile;
        }
        return null;
    }

    public static AProjectile Fire(EProjectileFlags projectileFlag, Vector3 srcPosition, Vector3 srcEulerAngle, Vector3 dstPosition)
    {
        ProjectilePool pool = m_projectileDict[projectileFlag];
        AProjectile projectile = pool.GetNextProjectile();
        if (projectile != null)
        {
            projectile.gameObject.SetActive(true);
            projectile.Fire(srcPosition, srcEulerAngle, dstPosition);
            return projectile;
        }
        return null;
    }

    public static AProjectile Fire(EProjectileFlags projectileFlag, Vector3 srcPosition, Vector3 srcEulerAngle, Transform dstTransform, Transform socket)
    {
        ProjectilePool pool = m_projectileDict[projectileFlag];
        AProjectile projectile = pool.GetNextProjectile();
        if (projectile != null)
        {
            projectile.transform.SetParent(socket);
            projectile.gameObject.SetActive(true);
            projectile.Fire(srcPosition, srcEulerAngle, dstTransform);
            return projectile;
        }
        return null;
    }

    private static void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        foreach (KeyValuePair<EProjectileFlags, ProjectilePool> pair in m_projectileDict)
        {
            for (int i = 0; i < pair.Value.Projectiles.Length; i++)
            {
                pair.Value.Projectiles[i].gameObject.SetActive(false);
            }

        }
    }
}
